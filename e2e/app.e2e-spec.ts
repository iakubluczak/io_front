import { IoappPage } from './app.po';

describe('ioapp App', () => {
  let page: IoappPage;

  beforeEach(() => {
    page = new IoappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
