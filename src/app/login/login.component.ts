<<<<<<< HEAD
import { Component, OnInit } from '@angular/core';
import { Directive, ElementRef, Input } from '@angular/core';
import { Output, EventEmitter  } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  username : string;
  password : string;

  constructor(private router: Router) { }

  send(){
    this.router.navigate(['./poker']);
  }
  
  ngOnInit() {
      console.log('Login');
  }

}
=======
// /**
//  * Created by adamo on 12.04.17.
//  */
// import { Component, OnInit } from '@angular/core';
// import { Router, ActivatedRoute } from '@angular/router';
//
// import { AlertService, AuthenticationService } from '../_services/index';
//
// @Component({
//     moduleId: module.id,
//     templateUrl: 'login.component.html'
// })
//
// export class LoginComponent implements OnInit {
//     model: any = {};
//     loading = false;
//     returnUrl: string;
//
//     constructor(
//         private route: ActivatedRoute,
//         private router: Router,
//         private authenticationService: AuthenticationService,
//         private alertService: AlertService) { }
//
//     ngOnInit() {
//         // reset login status
//         this.authenticationService.logout();
//
//         // get return url from route parameters or default to '/'
//         this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
//     }
//
//     login() {
//         this.loading = true;
//         this.authenticationService.login(this.model.username, this.model.password)
//             .subscribe(
//                 data => {
//                     this.router.navigate([this.returnUrl]);
//                 },
//                 error => {
//                     this.alertService.error(error);
//                     this.loading = false;
//                 });
//     }
// }
>>>>>>> 49adf3970de203cf97d874473fcae8c737fc5a1e
