import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-poker',
  templateUrl: './poker.component.html',
  styleUrls: ['./poker.component.css']
})
export class PokerComponent implements OnInit {

  constructor() { }

  players = [
    {id: 1, name:'Jakub', room:1},
    {id: 2, name:'Krzysztof', room:2},
    {id: 3, name:'Adam', room:3},
    {id: 4, name:'Kuba', room:1},
    {id: 5, name:'Michał', room:2}
  ];


  ngOnInit() {
  }

}
